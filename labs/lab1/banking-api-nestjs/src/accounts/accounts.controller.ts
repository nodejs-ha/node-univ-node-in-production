import { Controller, Get, Param } from '@nestjs/common';

// Path : domain name/accounts
@Controller('accounts')
export class AccountsController {

    @Get()
    findAll(): string {
        return 'find all accounts';
    }

    @Get(':id')
    findOne(@Param('id') id): string {
        return `find account with id: ${id}`;
    }
}
